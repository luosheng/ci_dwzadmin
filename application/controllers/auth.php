<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author: yuyongchao
 * Date:   2014-05-11 16:13:59
 * Last Modified by:   yuyongchao
 * Last Modified time: 2014-05-11 16:46:11
 * coding: utf-8
 */

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('auth_model', 'auth');
	}

	public function index() {
		$roles = $this->auth->get_roles();
		$this->_data['roles'] = $roles;
		$this->load->view('auth/index.html', $this->_data);
	}

	public function  get_nodes() {
		$node_list = $this->auth->get_nodes();

		$nodes = node_merge($node_list);

		$this->_data['nodes'] = $nodes;
		$this->load->view('auth/nodes.html', $this->_data);
	}

	public function set_access() {
		$is_handle = $this->input->post('post');
		$rid = $this->uri->segment(3);

		if ($is_handle) {
			$checked_inputs = $this->input->post('rules');

			$data = array();

			foreach ($checked_inputs as $node) {
				$data[] = array(
					'rid' => $rid,
					'nid' => $node
					);
			}

			$status = $this->auth->set_access($rid, $data);
			if ($status) {
				ajax_response(200,'权限分配成功', site_url('auth/index'),'forward');
			}
		} else {
			$nodes = $this->auth->get_nodes();

			$a_access = array();
			$r_access = $this->auth->get_access($rid);
			if ($r_access) {
				foreach ($r_access as $key => $value) {
					$a_access[] = $value['nid'];
				}
			}

			$access = node_merge($nodes, $a_access);
			$this->_data['nodes'] = $access;
			$this->_data['rid'] = $rid;
			$this->load->view('auth/access.html', $this->_data);
		}
	}


	public function role() {
		$rid = $this->uri->segment(3);

		if (empty($rid)) {
			$this->_add_role();
		} else {
			$this->_edit_role($rid);
		}
	}

	//权限配置
	public function node() {
		$nid = $this->uri->segment(3);

		if(empty($nid)) {
			$this->_add_node();
		} else {
			$this->_edit_node($nid);
		}

	}

	public function add_node() {
		$pid = $this->uri->segment(3);

		$this->_add_node($pid);
	}

	/**
	 * 删除角色
	 * @return [type] [description]
	 */
	public function del() {
		$rid = $this->uri->segment(3);
		$rs = $this->auth->del_role($rid);
		if ($rs) {
			ajax_response(200, '删除成功', 'auth/index');
		}

	}

	private function _add_role() {
		$this->load->library('form_validation');
		$status =$this->form_validation->run('group');

		if (false == $status) {
			$is_ajax = $this->input->post('isajax');
			if ($is_ajax) {
				$message = validation_errors();
				ajax_response(300, $message);
			}

			$this->load->view('auth/role.html');
		} else {
			$data = array(
				'rname' =>$this->input->post('rname'),
				'state' =>$this->input->post('state'),
				);

			$gid = $this->auth->add_role($data);
			if (!$gid) {
				ajax_response(300, '添加角色失败');
			} else {
				ajax_response(200, '添加角色成功');
			}
		}
	}


	private function _edit_role($rid) {
		$this->load->library('form_validation');
		$status =$this->form_validation->run('group');
		if (false == $status) {
			$this->_data['role'] = $this->auth->get_role($rid);
			$this->load->view('auth/role.html', $this->_data);
		} else {

		}
	}

	/**
	 * 添加权限节点
	 */
	private function _add_node($pid=0) {
		$this->load->library('form_validation');
		$status =$this->form_validation->run('auth');

		if (false == $status) {
			$is_ajax = $this->input->post('isajax');
			if ($is_ajax) {
				$message = validation_errors();
				ajax_response(300, $message);
			}
			$this->_data['pid'] = $pid ? $pid : 0;
			$this->_data['action'] = site_url('auth/add_node');
			$this->load->view('auth/node.html', $this->_data);
		} else {
			$data = array(
				'name' =>$this->input->post('name'),
				'title' =>$this->input->post('title'),
				'state' => $this->input->post('state'),
				'sort' => $this->input->post('sort'),
				'pid' => $this->input->post('pid'),
				);

			$gid = $this->auth->add_node($data);
			if (!$gid) {
				ajax_response(300, '添加节点失败');
			} else {
				ajax_response(200, '添加节点成功',site_url('auth/get_nodes'),'forward');
			}
		}
	}

	/**
	 * 编辑权限节点
	 * @param  [type] $nid [description]
	 * @return [type]      [description]
	 */
	private function _edit_node($nid) {
		$this->load->library('form_validation');
		$status =$this->form_validation->run('auth');

		if (false === $status) {
			$node = $this->auth->get_node($nid);
			$this->_data['node'] = $node;
			$this->_data['pid'] = $node['pid'];
			$this->_data['action'] = site_url('auth/node/'.$nid);
			$this->load->view('auth/node.html', $this->_data);
		} else {
			$data = array(
				'name' =>$this->input->post('name'),
				'title' =>$this->input->post('title'),
				'state' => $this->input->post('state'),
				'sort' => $this->input->post('sort'),
				'pid' => $this->input->post('pid'),
				);

			$result = $this->auth->edit_node($nid, $data);
			ajax_response(200, '更新节点成功',site_url('auth/get_nodes'),'closeCurrent','nodeList');
		}
	}


}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */