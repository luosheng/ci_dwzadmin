<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author: yuyongchao
 * Date:   2014-05-11 14:03:19
 * Last Modified by:   yuyongchao
 * Last Modified time: 2014-05-11 14:11:27
 * coding: utf-8
 */


class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('auth_model', 'auth');
		$this->load->model('admin_model', 'admin');

	}

	public function index() {
		$this->load->view('admin.html');
	}

	/**
	 * 账号管理
	 * @return [type] [description]
	 */
	public function manage_admin() {
		
		$uid = $this->uri->segment(3);

		$admins = $this->admin->get_admin();

		$admin_data = array();
		if ($admins) {
			foreach ($admins as $key => $admin) {
				$admin_data[$key]['username'] = $admin['username'];
				$admin_data[$key]['uid'] = $admin['uid'];
				$admin_data[$key]['rid'] = $admin['rid'];
				$role = $this->auth->get_role($admin['rid']);
				$admin_data[$key]['rname'] = $role['rname'];
			}
		}

		$this->_data['admins'] = $admin_data;
		$this->load->view('admin/admin.html', $this->_data);
	}

	public function add_admin() {
		$this->load->library('form_validation');
		$status = $this->form_validation->run('admin');

		$roles = $this->auth->get_roles();

		if (false === $status) {
			$is_ajax = $this->input->post('isajax');
			if ($is_ajax) {
				$message = validation_errors();
				ajax_response(300, $message);
			}

		} else {
			$passwd = md5($this->input->post('password'));
			$repasswd = md5($this->input->post('repasswd'));

			if ($passwd != $repasswd) {
				ajax_response(300, '两次密码不一致');
			}

			$data = array(
				'username' => $this->input->post('username'),
				'password' => $passwd,
				'rid' => $this->input->post('rid')
				);
			$uid = $this->admin->add_admin($data);

			if ($uid) {
				ajax_response(200, '添加成功');
			}
		}

		$this->_data['action'] = 'admin/add_admin';
		$this->_data['roles'] = $roles;

		$this->load->view('admin/admin_form.html', $this->_data);
	}


	public function edit_admin() {
		$this->output->enable_profiler(TRUE);
		$uid = $this->uri->segment(3);

		$admin = $this->admin->get_admin($uid);
		$roles = $this->auth->get_roles();

		$this->load->library('form_validation');
   
		$this->form_validation->set_rules('username', 'Username', 'required');

		if($this->form_validation->run() == FALSE) {
			$is_ajax = $this->input->post('isajax');
			if ($is_ajax) {
				$message = validation_errors();
				ajax_response('300', $message);
			}
		} else {
			$data = array(
				'username' => $this->input->post('username'),
				'rid' => $this->input->post('rid')
				);
			$uid = $this->input->post('uid');
			$return = $this->admin->edit_admin($data, $uid);

			if ($return) {
				ajax_response(200, '修改成功');
			}
		}

		$this->_data['action'] = 'admin/edit_admin';
		$this->_data['admin'] = $admin;
		$this->_data['roles'] = $roles;
		$this->_data['uid'] = $uid;
		$this->load->view('admin/admin_form.html', $this->_data);
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
