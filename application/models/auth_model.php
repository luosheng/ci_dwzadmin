<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	/**
	 * 添加节点
	 * @param [type] $data [description]
	 */
	public function add_node($data) {
		$rid = $this->db->insert('node', $data);
		return $rid;
	}

	/**
	 * 获取所有节点列表
	 * @return [type] [description]
	 */
	public function get_nodes() {
		$all_nodes = $this->db->get('node')->result_array();
		return $all_nodes;
	}

	/**
	 * 获取节点列表
	 * @param  integer $uid [description]
	 * @return [type]       [description]
	 */
	public function uid_nodes($uid = 0) {
        //读取用户所属用户组
        $groups = $this->get_groups($uid);

        $ids = array();
        foreach ($groups as $g) {
            $ids = array_merge($ids, explode(',', trim($g['rules'], ',')));
        }
        $ids = array_unique($ids);
        if (empty($ids)) {
            $_authList[$uid] = array();
            return array();
        }

     	$rules = $this->db->from('auth_rule')
                ->where('status', 1)
                ->where_in('id', $ids)->get()->result_array();
		return $rules;
	}

	public function get_groups($uid =0) {
		static $groups = array();
		$this->db->from('auth_group_access a');
		$this->db->join('auth_group g', 'a.group_id=g.id');
		$this->db->where(array('a.uid'=>$uid, 'g.status'=>1));

		$user_groups = $this->db->get()->result_array();

		$groups[$uid]=$user_groups?$user_groups:array();
        return $groups[$uid];
	}

	/**
	 * 获取单个节点信息
	 * @param  [type] $rid [description]
	 * @return [type]      [description]
	 */
	public function get_node($nid) {
		$data = $this->db->get_where('node', array('nid'=>$nid))->row_array();
		return $data;
	}

	/**
	 * 编辑节点
	 * @param  [type] $rid  [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function edit_node($nid, $data) {
		$status = $this->db->update('node', $data, array('nid'=>$nid));
		return $status;
	}

	/**
	 * 添加角色分组
	 * @param [type] $data [description]
	 */
	public function add_role($data) {
		$gid = $this->db->insert('role', $data);
		return $gid;
	}

	/**
	 * 编辑角色分组
	 * @param  [type] $gid [description]
	 * @return [type]      [description]
	 */
	public function edit_role($rid, $data) {
		$status = $this->db->update('role', $data, array('rid'=>$rid));
		return $status;

	}

	/**
	 * 删除角色
	 * @param  [type] $rid [角色ID]
	 * @return [type]      [description]
	 */
	public function del_role($rid) {
		return $this->db->delete('role', array('rid'=>$rid));
	}

	/**
	 * 调取角色列表
	 * @return [type] [description]
	 */
	public function get_roles() {
		$roles = $this->db->get('role')->result_array();
		return $roles;
	}

	/**
	 * 获取角色详细信息
	 * @param  [type] $gid [description]
	 * @return [type]      [description]
	 */
	public function get_role($gid) {
		$data = $this->db->get_where('role', array('rid'=> $gid))->row_array();
		return $data;
	}

	/**
	 * 设置角色权限
	 * @param [type] $gid   [description]
	 * @param [type] $rules [description]
	 */
	public function set_access($rid, $rules) {

		//清空原始权限
		$this->db->delete('access', array('rid'=>$rid));

		$status = $this->db->insert_batch('access', $rules);
		return $status;
	} 

	/**
	 * 获取角色原有权限
	 * @param  [type] $gid [description]
	 * @return [type]      [description]
	 */
	public function get_access($rid) {
		$data = $this->db->select('nid')->get_where('access', array('rid' => $rid))->result_array();
		return $data;
	}


}

/* End of file auth_model.php */
/* Location: ./application/models/auth_model.php */