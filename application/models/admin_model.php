<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function add_admin($data) {
		$user = array(
			'username' => $data['username'],
			'password' => $data['password']
			);
		$this->db->insert('user', $user);
		$uid = $this->db->insert_id();

		if ($uid) {
			$rid = $data['rid'];
			$user_role = array(
				'uid' => $uid,
				'rid' => $rid
				);
			$this->db->insert('user_role', $user_role);
		}

		return $uid;
	}

	public function edit_admin($data, $uid) {
		$user = array('username'=>$data['username']);
		$result = $this->db->update('user', $user, array('uid'=>$uid));

		if ($result) {
			$user_role = array('rid'=>$data['rid']);
	
			$return = $this->db->update('user_role', $user_role, array('uid'=>$uid));
		}

		return true;
	}

	public function get_admin($uid=0) {
		$this->db->from('user u');
		$this->db->join('user_role r', 'u.uid=r.uid');

		if ($uid) {
			$this->db->where('u.uid', $uid);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}

}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */