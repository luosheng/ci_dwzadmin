<?php
/**
 * Created By: ST2
 * Created on 2014-02-22 16:21:41
 * author: yongchao.yu@kunlun-inc.com
 * coding: utf-8
*/
$config = array(
	'category' => array(
		array(
			'field' => 'cname',
			'label' => '栏目名称',
			'rules' => 'required'
			),

		),
	'article' => array(
		array(
			'field' => 'title',
			'label' => '文章标题',
			'rules' => 'trim|required|min_length[4]'
			),
		array(
			'field' => 'type',
			'label' => '类型',
			'rules' => 'required|integer'
			),
		array(
			'field' => 'cid',
			'label' => '栏目',
			'rules' => 'required|integer'
			),
		),
	'auth' => array(
		array(
			'field' => 'name',
			'label' => '方法名称',
			'rules' => 'required'
			),
		array(
			'field' => 'title',
			'label' => '方法标题',
			'rules' => 'required'
			),
		array(
			'field' => 'state',
			'label' => '状态',
			'rules' => 'required|integer'
			),
		),
	'group' => array(
		array(
			'field' => 'rname',
			'label' => '角色名称',
			'rules' => 'required'
			),
		),
	'admin' => array(
		array(
			'field' => 'username',
			'label' => '用户名',
			'rules' => 'required'
			),
		array(
			'field' => 'rid',
			'label' => '用户组',
			'rules' => 'required|integer'
			),
		array(
			'field' => 'password',
			'label' => '密码',
			'rules' => 'required'
			),
		),

	);