<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	/**
     * 日期数字转中文
     * 用于日和月、周
     * @static
     * @access public
     * @param integer $number 日期数字
     * @return string
     */
    if (!function_exists('numberToCh')) {
	    function  numberToCh($number) {
	        $number = intval($number);
	        $array  = array('一','二','三','四','五','六','七','八','九','十');
	        $str = '';
	        if($number  ==0)  { $str .= "十" ;}
	        if($number  <  10){
	           $str .= $array[$number-1] ;
	        }
	        elseif($number  <  20  ){
	           $str .= "十".$array[$number-11];
	        }
	        elseif($number  <  30  ){
	           $str .= "二十".$array[$number-21];
	        }
	        else{
	           $str .= "三十".$array[$number-31];
	        }
	        return $str;
	    }
    }


    function ajax_response($statusCode, $message, $forwardUrl='', $callbackType='closeCurrent', $navTabId='', $rel='') {
		echo json_encode(
			array(
				'statusCode'=>$statusCode,
				'message'=> $message,
				'navTabId'=> $navTabId,
				'rel'=> $rel,
				'callbackType'=>$callbackType,
				'forwardUrl' => $forwardUrl
				)
		);
		exit();
    }


    function node_merge($node,$access=null,$pid=0){
		//print_r($node);die;
		$arr=array();
		foreach($node as $v){
		//print_r($v);die;
			if(is_array($access)){
				$v['access']=in_array($v['nid'],$access)?1:0;
			
			}
			if($v['pid']==$pid){
				$v['child']=node_merge($node,$access,$v['nid']);
				$arr[]=$v;
			
			}
		}
		return $arr;
	}//end

	/**
	 * 数组打印
	 * @param  [type] $arr [description]
	 * @return [type]      [description]
	 */
    function p($arr) {
    	echo '<pre>';
    	print_r($arr);
    }


/* End of file common_helper.php */
/* Location: ./application/helpers/common_helper.php */